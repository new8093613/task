/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      width: {
        "70%": "70%",
        "30%": "30%",
      },
      fontFamily: {
        Mulish: "Mulish, sans-serif",
      },
      height: {
        "1px": "1px",
      },
      colors: {
        additionColor: "#6200EE",
        pagesBgColor: "#EFE6FD",
        pagesColor: "#9654F4",
        bgColor: "rgba(0, 0, 0, 0.5)",
      },
      margin: {
        747: "-747px",
        740: "-740px",
      },
    },
  },
  plugins: [],
};
