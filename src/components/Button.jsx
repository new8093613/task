import React from 'react'

const Button = ({children,onClick}) => {
    const className ="border border-transparent w-full  bg-additionColor text-white rounded-md py-2.5 font-medium fs-base font-Mulish duration-300 hover:bg-white hover:border-additionColor hover:text-additionColor hover:duration-300";
   
  return (
    <button onClick={onClick} className={className}>{children}</button>
  )
}

export default Button