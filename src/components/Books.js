import React from 'react'
import useAxios from '../hooks/useAxios'
import Book from './Book'

const Books = ({deleteBook,editBook}) => {
  const { res, loader, error } = useAxios("/books")
  return (
    <>
    {
        loader ? (<div className="spinner-border text-primary" role="status" >
          <span className="visually-hidden">Loading...</span>
        </div>):(res?.map((item,index)=><Book item={item} deleteBook={deleteBook} editBook={editBook}/>))
    }
    </>
  )
}

export default Books